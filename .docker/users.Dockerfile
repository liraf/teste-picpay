FROM picpay/php:7.2-fpm-base

ARG USER_UID=33
ARG USER_GID=33

COPY ./support/docker/config/       /

RUN pecl install -f xdebug \
    && docker-php-ext-enable xdebug \
    && docker-php-source delete \
    && { \
      echo "xdebug.remote_enable=on"; \
      echo "xdebug.remote_autostart=on"; \
      echo "xdebug.remote_port=9001"; \
      echo "xdebug.remote_handler=dbgp"; \
      echo "xdebug.remote_connect_back=0"; \
      echo "xdebug.idekey=local"; \
      echo "xdebug.remote_host=172.77.1.1"; \
    } >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && deluser www-data \
    && addgroup -g $USER_GID www-data \
    && adduser -u $USER_UID -G www-data -g 'www-data' -s /sbin/nologin -D  www-data \
    && chmod +x /start.sh

# entrypoint
ENTRYPOINT ["sh", "-c"]

EXPOSE 80

# start
CMD ["/start.sh"]
