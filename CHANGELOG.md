# Change Log

## [0.0.2] - 2020-05-21

Aproveitei e removi mongodb, instalei o xdebug e configurei uma rede fixa no docker-compose

### Adicionado

- Suporte ao XDEBUG

### Alterado

- Removido suporte ao Mongo DB
- Configurei uma rede fixa 172.77.1.0/24 no Docker Compose

### Ajustado

- Ajustado o erro handler para Throwable
- Ajustado arquivo do Change Log
    - https://lumen.laravel.com/docs/7.x/upgrade

## [0.0.1] - 2020-05-21

Peguei o arquivo padrão do teste do PicPay, atualizei as dependências do PHP e ajustei a estrutura para algo, que penso ser mais organizado.

### Adicionado

- Nada foi adicionado

### Alterado

- Estrutura de pastas
    - Arquivos do Docker estão na pasta .docker
    - Aplicação Lumen/Laravel movida para users-app
- Dockerfile do PHP
    - Colocado um ARG para UID e GID
- docker-compose.yaml
    - Inseridos ARGS para UID e GID
- Inseridos arquivos padrão
    - .editorconfig
    - .gitignore
    - LICENSE
    - CHANGELOG

### Ajustado

- Nada foi ajustado
